#include <iostream>
#include <ncurses.h>
#include <zconf.h>
#include "Menu.h"
#include "Normal.h"
#include "Riddle.h"
#include <string>

using namespace std;
void start_game ();// function to display the logo in the start of the game
void color_control();// exception handling to check for the color

int main (){
    system("clear");// to clear the screen
    color_control();
    start_game();// using the function
    Menu menu;// creating an object for the menu
    /*from the function Menu we get 1 for the normal mode
     * 2 for the riddle mode
     * 3 for instructions
     * 4 for credits
     * and 5 to exit */
    while(true){
        int mode = menu.First_menu();// displaying the staring menu
        int answer;// the answer for the levels
        if (mode == 1){// 1 is the normal game
            answer = menu.Normal_mode();
            Normal game;// creating a object of the normal class
            if (answer == 3){// for the levels
                game.level_3();
            }
            else if (answer == 1){
                game.level_1();
            }
            else if (answer == 2) {
                game.level_2();
            }
        }
        else if (mode == 2){// 2 is the riddle game
            answer = menu.Riddle_mode();
            Riddle game_r;// creating an object on the riddle class
            if (answer == 3){
               game_r.level_3();
            }
            else if (answer == 1){
                game_r.level_1();
            }
            else if (answer == 2){
                game_r.level_2();
            }
        }
        else if (mode == 3){// 3 is to see the instructions
            menu.instructions();// displaying the instructions
        }
        else if (mode == 4){// 4 is to see the credits
            menu.Credits();// displaying the credits
        }
        else{// else the user chooce to exit
            system("clear");// clears the terminal
            cout << "Thanks for playing my game"<<endl;
            usleep(2000000);// pauses so the user will see the message
            break;
        }
    }
    return 0;


}

void start_game (){
  // games name
  string  name1 = "\t\t▄████  ████▄ █▄▄▄▄   ▄▀  ████▄    ▄▄▄▄▀ ▄▄▄▄▀ ▄███▄      ▄               █ ▄▄  ██     ▄▄▄▄▀ ▄  █    ▄▄▄▄▄";
  string  name2 = "\t\t█▀   ▀ █   █ █  ▄▀ ▄▀    █   █ ▀▀▀ █ ▀▀▀ █    █▀   ▀      █              █   █ █ █ ▀▀▀ █   █   █   █     ▀▄";
  string  name3 = "\t\t█▀▀    █   █ █▀▀▌  █ ▀▄  █   █     █     █    ██▄▄    ██   █             █▀▀▀  █▄▄█    █   ██▀▀█ ▄  ▀▀▀▀▄";
  string  name4 = "\t\t█      ▀████ █  █  █   █ ▀████    █     █     █▄   ▄▀ █ █  █             █     █  █   █    █   █  ▀▄▄▄▄▀";
  string  name5 = "\t\t █             █    ███          ▀     ▀      ▀███▀   █  █ █              █       █  ▀        █";
  string  name6 = "\t\t  ▀           ▀                                       █   ██               ▀     █           ▀";
  string  name7 = "\t\t                                                                                ▀";
  cout << name1<<endl<<name2 <<endl<<name3<<endl<<name4<<endl<<name5<<endl<<name6<<endl<<name7<<endl;

    cout <<"\n\n\n\n \t\t\t\t\t\t\t";// just to direct in the right space
    string welcome = "Welcome to my maze game";
    string owner = "Developed by Nick Dimitriou";
    for (int i = 0; i < welcome.length(); i++){
        cout << welcome[i]<< flush;// printing a letter and then  sleeping for a bit
        usleep(60000);
    }
    cout <<endl<<endl<< "\t\t\t\t\t\t\t";
    for (int i = 0; i < owner.length(); i++){
        cout << owner[i]<< flush;// same as before
        usleep(60000);
    }
    cout<<endl << endl <<endl << endl <<endl;
    cout << "Press enter to start the game"<<endl;
    cin.get();
}

void color_control(){
    // using exception handling to make sure that the terminal supports colors bacause not every terminal does
    try {
        initscr();
        if (!(has_colors())) {
            string error = "Your terminal does not support colors pls find a terminal that does";
            throw error;
        }
    }
    catch (string message){
        cout<<message<<"I recommend using virtual box to install ubuntu (the operation system that the software was developed"<<endl;
    }
    endwin();
}
