//
// Created by user on 3/24/19.
//

#ifndef MAZE_RIDDLE_H
#define MAZE_RIDDLE_H

#include <ncurses.h>
#include <string>
#include "Normal.h"


class Riddle: public Normal {
public:
    // the level that will be used in the main fucntions
    void level_1();
    void level_2();
    void level_3();

private:
    WINDOW * riddle_win;// a window for the riddle
    bool riddle_question(std::string question, std::string answer);// a method to displaying the riddle
    void movement(int finishi, int finishj, int sizei, int sizej,std::string terrain[], int riddle_posi,int riddle_posj,std::string question, std::string answer);// we need to ovewrite this method because there is the riddles that work differently
};


#endif //MAZE_RIDDLE_H
