//
// Created by user on 3/24/19.
//

#include "Normal.h"
#include "Riddle.h"
#include <string>
#include <ncurses.h>
#include <zconf.h>

bool Riddle::riddle_question(std::string question, std::string answer) {
    int row,col;// to store the max colums and max rows
    getmaxyx(stdscr,row,col);//getting the max rows and colons
    riddle_win = newwin(10,col,0,0);// creating a new window to display the riddle
    refresh();
    std::string user_answer;
    std::string user_answer2;// because ncurses indereact weird with strinfs i have to create 2 to fix the problem
    echo();// to display the answer because we had use noecho which means that the user would not be able to see what he has writing
    do{
        wclear(riddle_win);
        box(riddle_win,0,0);// to have a box around the widnow
        mvwprintw(riddle_win,1,1,question.c_str());//displaying the question
        mvwprintw(riddle_win,8,1,"Input answer: ");
        wscanw(riddle_win,"%s",user_answer.c_str());
        user_answer2 = user_answer.c_str();
        wrefresh(riddle_win);
        if ((user_answer2.compare(answer)) && (user_answer2.compare("back"))) {
            wclear(riddle_win);
            box(riddle_win, 0, 0);
            mvwprintw(riddle_win, 1, 1, "WRONG !!! Try again");// if the answer is wrong print that it is wrong
            wrefresh(riddle_win);
            usleep(1000000);// and pause for a bit for the user to read the message
        }
    }while ((user_answer2.compare(answer)) && (user_answer2.compare("back")));// doing the same proccess until the user finds the answser of types back
    wclear(riddle_win);
    wrefresh(riddle_win);
    delwin(riddle_win);// deleting the window cause we finished with the riddle
    if ((user_answer2.compare(answer))){
        return false;// return false if the user found the question
    }
    else{
        return true;// retunr true if the user used back
    }
}


void Riddle::movement(int finishi, int finishj, int sizei, int sizej,std::string terrain[], int riddle_posi,int riddle_posj, std::string question, std::string answer){
    int key;// to store the key that the user pressed
    bool guard = true; // to make sure that the riddle is displayed one time
    keypad(stdscr, TRUE);// to be able to use the arrow keys and enter
    int posi = 0;
    int posj = 0;
    while ((posi != finishi) || (posj != finishj)) {
        print_map(sizei,terrain);
        getpos(sizei,sizej,terrain,posi, posj);
        key = getch();// get one character
        mvprintw(1,1,"                              ");// deleting the message
        switch (key) {
            case KEY_RIGHT:
                move_right(posi,posj,terrain);
                break;
            case KEY_LEFT:
                move_left(posi, posj,terrain);
                break;
            case KEY_DOWN:
                move_down(posi, posj,terrain);
                break;
            case KEY_UP:
                move_up(posi, posj,terrain);
                break;
        }
        if (key == 10) {
            break;
        }
        int preposi = posi;
        int preposj = posj;
        getpos(sizei,sizej,terrain, posi, posj);
        //using the guard in order to know if the user answered the question and if he did to stop diplaying the R in the map
        if ((posi == riddle_posi) && (posj == riddle_posj)&& guard){
            if (riddle_question(question,answer)){
                guard = false;
            }
            else{
                terrain[posi].replace(posj,1,std::string("R"));
                terrain[preposi].replace(preposj,1,std::string("@"));
             }
        }
    }
    clear();
    if (key != 10){// to check that the user did not just pressed enter to go back to the menu
        mvprintw(1,1,"YOU ESCAPED, press any character to go back");
        getch();
    }
}
void Riddle::level_1() {
    std::string terrain[6] = {{"######"},// an easy level
                                     {"#@ ###"},
                                     {"## ###"},
                                     {"## ###"},
                                     {"##R  0"},
                                     {"######"}};
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(4,5,6,6,terrain,4,2,"David's father has three sons : Snap, Crackle and _____ ?","david");
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}

void Riddle::level_2() {
    std::string terrain[12] = {{"############"},//an modarate level
                               {"##@       ##"},
                               {"#  ###### ##"},
                               {"######### ##"},
                               {"######### ##"},
                               {"##  R     ##"},
                               {"## #########"},
                               {"##  ########"},
                               {"##    ######"},
                               {"## ## ######"},
                               {"#  ##      0"},
                               {"############"}};
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(10,11,12,12,terrain,5,4,"Everyone has me but nobody can lose me. I am a _____ ","shadow");
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}

void Riddle::level_3() {
    std::string terrain[20] = {{"####################"},// an difficult level
                               {"####### ####       #"},
                               {"####### #### ##### #"},
                               {"####### #### ##### #"},
                               {"0  #### #### ##### #"},
                               {"## #### #### ##### #"},
                               {"## #### #### ##### #"},
                               {"## ####      ##### #"},
                               {"## ############### #"},
                               {"##R############### #"},
                               {"#    #   @         #"},
                               {"#  ############### #"},
                               {"# ################ #"},
                               {"# ################ #"},
                               {"# ####    ######## #"},
                               {"# #### ## ######## #"},
                               {"# #### ## ######## #"},
                               {"# #### ## ######## #"},
                               {"#      ##          #"},
                               {"####################"}};
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(4,0,20,20,terrain,9,2,"The more you take, the more you leave behind. What am i _____ ?","footsteps");
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}