//
// Created by user on 3/20/19.
//

#include "Normal.h"
#include <ncurses.h>
#include <string>
#include <string.h>


void Normal::print_map(int sizei, std::string terrain[]) {
    int row,col;// to store the max rows and colums
    getmaxyx(stdscr,row,col);//getting the max rows and colons
    for (int i =0; i<sizei; i++){
        mvprintw((row / 2)+i-3 , col/2 , terrain[i].c_str());// pritning the array
    }
    mvprintw(row-1,1,"Press enter if you want to go back to main menu");// and the message
}


void Normal::getpos(int sizei ,int sizej,std::string terrain[], int &posi, int &posj) {
    // going throw the array to fing the @
    for (int i = 0; i < sizei; i++){
        for (int j = 0; j < sizej; j++){
            if (terrain[i][j] == '@'){
                // and storing its location
                posi = i;
                posj = j;
            }
        }
    }
}
/* the next 4 methods are the same
 * they check if there is a wall in the place the player request to go
 * if there is it prints a messgae
 * if there is not it changes the array puting the @ sign at the requested position
 * and a space in the previous*/
void Normal::move_down(int posi,int posj, std::string terrain[]){
        if (!(terrain[posi+1][posj]=='#')){
            terrain[posi].replace(posj,1,std::string(" "));
            terrain[posi+1].replace(posj,1,std::string("@"));
        }
        else{
            mvprintw(1,1,"Invalid move! A wall is there");
        }
    }


void Normal::move_left(int posi, int posj,std::string terrain[]) {
        if (!(terrain[posi][posj-1] == '#')){
            terrain[posi].replace(posj,1,std::string(" "));
            terrain[posi].replace(posj-1,1,std::string("@"));
        }
        else{
            mvprintw(1,1,"Invalid move! A wall is there");
        }
    }


void Normal::move_right(int posi, int posj,std::string terrain[]) {
        if (!(terrain[posi][posj+1] == '#')){
            terrain[posi].replace(posj,1,std::string(" "));
            terrain[posi].replace(posj+1,1,std::string("@"));
        }
        else{
            mvprintw(1,1,"Invalid move! A wall is there");
        }
    }

void Normal::move_up(int posi, int posj,std::string terrain[]) {
        if (!(terrain[posi-1][posj] == '#')){
            terrain[posi].replace(posj,1,std::string(" "));
            terrain[posi-1].replace(posj,1,std::string("@"));
        }
        else{
            mvprintw(1,1,"Invalid move! A wall is there");
        }
    }

void Normal::movement(int finishi, int finishj, int sizei, int sizej,std::string terrain[]) {
    int key;// to store the key that is pressed
    keypad(stdscr, TRUE);// to be able to use the arrow keys and enter
    int posi = 0;
    int posj = 0;
    while ((posi != finishi) || (posj != finishj)) {
        print_map(sizei,terrain);
        getpos(sizei,sizej,terrain, posi, posj);
        key = getch();// get one character
        mvprintw(1,1,"                              ");// deleting the message
        switch (key) {
            case KEY_RIGHT:
                move_right(posi, posj,terrain);
                break;
            case KEY_LEFT:
                move_left(posi, posj,terrain);
                break;
            case KEY_DOWN:
                move_down(posi, posj,terrain);
                break;
            case KEY_UP:
                move_up(posi, posj,terrain);
                break;
        }
        if (key == 10) {// for the case that user pressed enter
            break;
        }
        getpos(sizei,sizej,terrain, posi, posj);
    }
    clear();
    if (key != 10){// to check that the user did not press enter
        mvprintw(1,1,"YOU ESCAPED, press any character to go back");
        getch();
    }
}

void Normal::level_1() {
    std::string terrain[6] = {{"######"},// an easy level
            {"#@####"},
            {"# ####"},
            {"#   ##"},
            {"###  0"},
            {"######"}};

    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(4,5,6,6,terrain);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}

void Normal::level_2() {
    std::string terrain[12] = {{"############"},// an modarate level
                               {"##      ####"},
                               {"## #### ####"},
                               {"## # ##    #"},
                               {"## # ## ## #"},
                               {"#@   ## ## #"},
                               {"# ##### ## #"},
                               {"# ##### ## #"},
                               {"##      ## #"},
                               {"## #########"},
                               {"##         0"},
                               {"############"}};
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(10,11,12,12,terrain);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}

void Normal::level_3() {
    std::string terrain[20] = {{"####################"},// an difficult level
                               {"#@           #######"},
                               {"# ########## #######"},
                               {"# ######     #######"},
                               {"#     ##           #"},
                               {"##### ## ######### #"},
                               {"##    ##         # #"},
                               {"################ # #"},
                               {"##               # #"},
                               {"## # ###############"},
                               {"## # ####    #     0"},
                               {"## #      ## # #####"},
                               {"## ######### # #####"},
                               {"## ######### # #####"},
                               {"## ###       # #####"},
                               {"## ### ####### #####"},
                               {"## ###  ###### #####"},
                               {"## ########### #####"},
                               {"##                 #"},
                               {"####################"}};
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    movement(10,19,20,20,terrain);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
}
