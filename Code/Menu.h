//
// Created by user on 3/20/19.
//

#ifndef MAZE_MENU_H
#define MAZE_MENU_H

#include <string>

class Menu {
public:
    int First_menu();// the first menu that the user will see
    int Normal_mode();// the menu for the Normal mode
    int Riddle_mode();// the menu for the Riddle mode
    void Credits();// to see the credits
    void instructions();// to see the instructions
private:
    void print_menu(int option, int n_choices, std::string choices[]);// print the menu
    int movement(int n_choices,int option, std::string choices[]);// moves in the menu

};


#endif //MAZE_MENU_H
