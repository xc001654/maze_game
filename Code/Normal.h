//
// Created by user on 3/20/19.
//

#ifndef MAZE_NORMAL_H
#define MAZE_NORMAL_H

#include <string>

class Normal {
public:
    // the level that will be called from the main file
    void level_1();
    void level_2();
    void level_3();

protected:
    // protected because they will be used with inheritance in the riddle mode
    void print_map(int sizei, std::string terrain[]);// to print the array
    void getpos(int sizei,int sizej,std::string terrain[], int &posi,int  &posj);// to get the position of the @ sign using pointers because we need to return the position
    void move_up(int posi, int posj, std::string terrain[]);
    void move_down(int posi, int posj, std::string terrain[]);
    void move_right(int posi, int posj, std::string terrain[]);
    void move_left(int posi, int posj, std::string terrain[]);
    void movement(int finishi, int finishj, int sizei, int sizej, std::string terrain[]);// the basic method that most of the work is happening

};


#endif //MAZE_NORMAL_H
