//
// Created by user on 3/18/19.
//

#include "Menu.h"
#include <ncurses.h>
#include <string>
#include <string.h>

void Menu::print_menu(int option, int n_choices, std::string choices []) {
    start_color();// to be able to use colors
    int row,col,x;// to srore the max rows the max colums and to use x to manage the space
    x = 0;
    getmaxyx(stdscr,row,col);// getting max row and colums
    int space = col/n_choices;//saparaing the space into the number of the options to have equal spacing
    for (int i = 0; i < n_choices; i++){// printing all the option
        if (option == i + 1){// and the one that the oprion are in is blinking
            attron(A_REVERSE);
            mvprintw(row-1,x,"%s", choices[i].c_str());// making the current option blinking
            attroff(A_REVERSE);
        }
        else{
            mvprintw(row-1,x,"%s", choices[i].c_str());//printing the other choices normally
        }
        x = x + space;// adding some space to the options
    }
    refresh();
}

int Menu::movement(int n_choices,int option, std::string choices[]){
    int key;// to input key number
    while(TRUE) {
        key = getch();// get one character
        switch (key) {
            case KEY_RIGHT:
                if (option == n_choices) {// if we are in the last position
                    option = 1;// go to the first option
                } else {
                    option++;// go to the next option
                }
                break;
            case KEY_LEFT:
                if (option == 1) {// if we are in the first option
                    option = n_choices;// go to the last
                } else {
                    option--;
                }
                break;
            case 10:
                return option;// store the choice
        }
        print_menu(option, n_choices, choices);
    }
}

int Menu::First_menu() {
    std::string choices[] = { // creating the menu with all the modes
            "Normal game",
            "Riddle game",
            "Instructions",
            "Credits",
            "Exit",
    };
    int n_choices = 5;// the number of options
    int choice = 0; //the choice of the player
    int option = 1; //1 because it will start by the first option

    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    keypad(stdscr, TRUE);// to be able to use the arrow keys and enter
    print_menu(option,n_choices,choices);
    init_pair(1,COLOR_BLUE,COLOR_BLACK);// creatomg a color pair
    attron(A_BOLD | COLOR_PAIR(1));// to print the Instructions in bold and with color
    mvprintw(1,0,"Use the keys to move, and press enter to make your choice");
    attroff(A_BOLD | COLOR_PAIR(1));// disabling bold and color

    init_pair(2,COLOR_RED,COLOR_BLACK);// creatog a second color pair
    attron(COLOR_PAIR(2));// enabling second color pair
    std::string name1 = "  ______                    _   _                           _   _          ";
    std::string name2 = " |  ____|                  | | | |                         | | | |         ";
    std::string name3 = " | |__ ___  _ __ __ _  ___ | |_| |_ ___ _ __    _ __   __ _| |_| |__  ___  ";
    std::string name4 = " |  __/ _ \\| '__/ _` |/ _ \\| __| __/ _ \\ '_ \\  | '_ \\ / _` | __| '_ \\/ __| ";
    std::string name5 = " | | | (_) | | | (_| | (_) | |_| ||  __/ | | | | |_) | (_| | |_| | | \\__ \\ ";
    std::string name6 = " |_|  \\___/|_|  \\__, |\\___/ \\__|\\__\\___|_| |_| | .__/ \\__,_|\\__|_| |_|___/ ";
    std::string name7 = "                 __/ |                         | |                         ";
    std::string name8 = "                |___/                          |_|                         ";
    int row,col;
    getmaxyx(stdscr,row,col);//getting the max rows and colons
    // printing the name in the right position
    mvprintw(row/2-2,(col-strlen(name4.c_str()))/2,name1.c_str());
    mvprintw(row/2-1,(col-strlen(name4.c_str()))/2,name2.c_str());
    mvprintw(row/2,(col-strlen(name4.c_str()))/2,name3.c_str());
    mvprintw(row/2+1,(col-strlen(name4.c_str()))/2,name4.c_str());
    mvprintw(row/2+2,(col-strlen(name4.c_str()))/2,name5.c_str());
    mvprintw(row/2+3,(col-strlen(name4.c_str()))/2,name6.c_str());
    mvprintw(row/2+4,(col-strlen(name4.c_str()))/2,name7.c_str());
    mvprintw(row/2+5,(col-strlen(name4.c_str()))/2,name8.c_str());
    attroff(COLOR_PAIR(2));// disabling the color
    choice = movement(n_choices,option, choices);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
    return choice ;

}

int Menu::Normal_mode() {
    std::string choices[] = { // creating the menu with all the modes
            "Level 1",
            "Level 2",
            "Level 3",
            "Go back",
    };

    int n_choices = 4;// number of choices
    int option = 1; //1 because it will start by the first option
    int choice = 0; //to know when the player chooce

    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    keypad(stdscr, TRUE);// to be able to use the arrow keys and enter
    init_pair(1,COLOR_BLUE,COLOR_BLACK);// creating a color pair
    attron(A_BOLD | COLOR_PAIR(1));// to print the Instructions in bold and with color
    mvprintw(1,0,"Choose your level");
    attroff(A_BOLD | COLOR_PAIR(1));// disabling bold and color
    print_menu(option, n_choices, choices);
    choice = movement(n_choices,option,choices);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
    return choice ;
}

int Menu::Riddle_mode() {
    std::string choices[] = { // creating the menu with all the modes
            "Level 1",
            "Level 2",
            "Level 3",
            "Go back",
    };

    int n_choices = 4;// number of choices
    int option = 1; //1 because it will start by the first option
    int choice = 0; //to know when the player have n_choise

    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    keypad(stdscr, TRUE);// to be able to use the arrow keys and enter
    print_menu(option, n_choices, choices);
    init_pair(1,COLOR_BLUE,COLOR_BLACK);// creating a color pair
    attron(A_BOLD | COLOR_PAIR(1));// to print the Instructions in bold and with color
    mvprintw(1,0,"Choose your level");
    attroff(A_BOLD | COLOR_PAIR(1));// disabling bold and color
    choice = movement(n_choices,option,choices);
    clrtoeol();
    clear();
    refresh();// refresh the screen
    endwin();// close the curses mode
    return choice ;
}

void Menu::Credits() {
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    init_pair(1,COLOR_BLUE,COLOR_BLACK);// creatomg a color pair
    attron(A_BOLD | COLOR_PAIR(1));// to print the Instructions in bold and with color
    mvprintw(1,0,"Developed by: Nick Dimitriou");
    mvprintw(2,0,"Name inspired by: Anthony Varsamis");
    attroff(A_BOLD | COLOR_PAIR(1));// disabling bold and color
    mvprintw(5,0,"Press any key to go back");
    refresh();// refresh the screen
    getch();
    clear();
    endwin();// close the curses mode
}

void Menu::instructions() {
    int row, col;
    initscr();// starting the curses
    noecho();
    cbreak();// to disable line buffering
    init_pair(1,COLOR_BLUE,COLOR_BLACK);// creatomg a color pair
    attron(A_BOLD | COLOR_PAIR(1));// to print the Instructions in bold and with color
    getmaxyx(stdscr,row,col);
    mvprintw(1,1,"For normal mode:");
    mvprintw(1,col/2 - 20,"Use the arrow keys to move");
    mvprintw(2,col/2 - 20,"Avoid the walls and got to the 0 to win");
    mvprintw(5,1,"For riddle mode: ");
    mvprintw(6,col/2-20,"Same as normal");
    mvprintw(7,col/2-20,"But there is a R symbol in the map");
    mvprintw(8,col/2-20,"Which asks you a riddle");
    mvprintw(9,col/2-20,"And you have to answer correctly to proceed");
    mvprintw(10,col/2-20,"All answers must be in lower case");
    mvprintw(11,col/2-20,"Type back if you dont know the answer so you can explore a different path");
    attroff(A_BOLD | COLOR_PAIR(1));// disabling bold and color
    mvprintw(13,0,"Press any key to go back");
    refresh();// refresh the screen
    getch();
    clear();
    endwin();// close the curses mode
}